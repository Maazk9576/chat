<?php

namespace App\Events;

use App\Models\Chat;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

// class NewMessage
// {
//     use Dispatchable, InteractsWithSockets, SerializesModels;

//     /**
//      * Create a new event instance.
//      */
//     public function __construct()
//     {
//         //
//     }

//     /**
//      * Get the channels the event should broadcast on.
//      *
//      * @return array<int, \Illuminate\Broadcasting\Channel>
//      */
//     public function broadcastOn(): array
//     {
//         return [
//             new PrivateChannel('channel-name'),
//         ];
//     }
// }

class NewMessage implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $chat;
    // public $userId;

    public function __construct(Chat $chat)
    {
        $this->chat = $chat;
        // $this->userId = $userId;
    }

    public function broadcastOn()
    {
        return new Channel('chat');
    }
    // public function broadcastOn()
    // {
    //     // return new PrivateChannel('chat.' . $this->userId);
    //     return new Channel('chat');
    // }

    public function broadcastAs()
    {
        return 'new-message';
    }
}
