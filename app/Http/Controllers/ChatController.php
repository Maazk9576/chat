<?php

namespace App\Http\Controllers;

use App\Events\NewMessage;
use App\Models\Chat;
use App\Models\User;
use Illuminate\Http\Request;
use Pusher\Pusher;

class ChatController extends Controller
{
    public function index($id)
    {
        $sender_id = auth()->user()->id;
        $chats = Chat::where('receiver_id', $id)->where('sender_id', $sender_id)->get();
        // $chats .= Chat::where('receiver_id', $sender_id)->where('sender_id', $id)->get();
        return view('chat', compact('chats'));
    }

    public function sendMessage(Request $request)
    {
        $message = $request->input('message');
        $sender_id = auth()->user()->id;
        $receiver_id = $request->input('receiver_id');
        $receiver_name = User::where('id', $receiver_id)->first(['name']);
        // dd($receiver_name['name']);
        $chat = new Chat();
        $chat->message = $message;
        $chat->sender_id = $sender_id;
        $chat->receiver_id = $receiver_id;
        $chat->save();

        event(new NewMessage($chat));
        // broadcast(new NewMessage($chat, $receiver_id))->toOthers(); // This will broadcast the new message
        // event(new NewMessage($chat, $receiver_id));
        $pusher = new Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'),
            [
                'cluster' => env('PUSHER_APP_CLUSTER'),
                'useTLS' => true,
            ]
        );
        // $pusher->trigger('chat-channel', 'client-user-connected', ['message' => 'A user has connected to the chat!']);

        return response()->json(['success' => true]);
    }
}
