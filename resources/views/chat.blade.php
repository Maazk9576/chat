<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Chat</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
</head>

<body>
    <h1>Chat</h1>

    <div id="chat">
        @php

        @endphp
        @foreach ($chats as $chat)
            <p>{{ $chat->message }}</p>
        @endforeach
    </div>
    @php
        $id = request()->route('id');
    @endphp
    <form id="message-form">

        <input type="text" id="message" name="message">

        <input type="hidden" id="receiver_id" name="receiver_id" value="{{ $id }}">

        <button type="submit">Send</button>
    </form>

    <script src="https://js.pusher.com/7.0/pusher.min.js"></script>

    <script>
        const pusher = new Pusher('{{ env('PUSHER_APP_KEY') }}', {
            cluster: '{{ env('PUSHER_APP_CLUSTER') }}',
        });

        const userId = document.getElementById('receiver_id').value;
        // const channelName = 'private-chat-' + userId;
        // const channelName = 'chat-channel';
        // const channel = pusher.subscribe('chat-channel');
        // channel.bind('new-message', function(data) {
        //     console.log(data);
        //     // console.log(data.chat.message);
        //     document.getElementById('chat').innerHTML += `<p>${data.chat.message}</p>`;
        // });
        const channel = pusher.subscribe('chat');
        channel.bind('new-message', function(data) {
            document.getElementById('chat').innerHTML += `<p>${data.message}</p>`;
        });

        // channel.bind('client-user-connected', function(data) {
        //     document.getElementById('chat').innerHTML += `<p><strong>Notification:</strong> ${data.message}</p>`;
        // });


        document.getElementById('message-form').addEventListener('submit', function(e) {
            e.preventDefault();

            const message = document.getElementById('message').value;
            const receiver_id = document.getElementById('receiver_id').value;
            fetch('/send', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    body: JSON.stringify({
                        message,
                        receiver_id
                    })
                })
                .then(response => response.json())
                .then(data => {
                    document.getElementById('message').value = '';
                });
        });
        // fetch('/send', {
        //         method: 'POST',
        //         headers: {
        //             'Content-Type': 'application/json',
        //             'X-CSRF-TOKEN': '{{ csrf_token() }}'
        //         },
        //         body: JSON.stringify({
        //             message,
        //             receiver_id
        //         })
        //     })
        //     .then(response => response.json())
        //     .then(data => {
        //         document.getElementById('message').value = '';
        //     });
        // });
        // $(document).ready(function(event) {
        //     channel.trigger('client-user-connected', {
        //         message: 'A user has connected to the chat!'
        //     });
        // });
    </script>
</body>

</html>
